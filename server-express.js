var express = require('express');
var app = express();
var clienti = [{ nome: "Diego" }, { nome: "Matteo" }]

app.use(express.json());

app.get('/api/clienti', function (req, res) {
    res.status(200).contentType('application/json').send(clienti);
});

app.post('/api/clienti', function (req, res) {
    console.log(req.body);
    clienti.push(req.body);
    res.status(200).contentType('application/json').send(clienti);
});

app.listen(1337, function () {
    console.log('Example app listening on port 1337!');
});

